# CS 5348: Project 1 #

### Project Overview ###

The main objective of this project is to explore various capabilities and functions in Linux as preparation for what's to come in CS 5348: Operating Systems.

This project consists of two parts: 1) Creating and executing a shell script and makefile with given C and input files that calculate the roots of a degree-2 polynomial and certain relative primes, and 2) Creating and executing a shell script that traverses and prints each directory from a starting file, as well as each file < 1 KB in size.

### Files and Functions ###

* cal.c, square.c, quadratic.c - C files provided for this project (compiled using the gcc compiler)
* cal.in - input file for calculations involving cal.c, square.c, and quadratic.c
* makefile - used for compiling cal.c, square.c, and quadratic.c into their respective object files (cal.o, square.o, and quadratic.o)
* cal.exe - executable for cal.c and associated C files
* exec.sh - shell script for redirecting input from cal.in and redirecting output to a file 'f'
* f - output file produced by exec.sh
* d - input file for traverse.sh (the input is the home directory (~), since that's probably the only directory common to the TA's and my Linux environments.
* traverse.sh - shell script for traversing directories from an input directory (provided by an input file 'd')
* l - log file for listing files of size <1 KB (found in home directory after executing traverse.sh)
* clearAll - makefile that clears everything for Part 1 in case you need to run it again (run by typing 'make -f clearAll')

### Execution Instructions ###

1) To run Part 1, type 'bash exec.sh'.
2) To run Part 2, type 'bash traverse.sh'.
