make # Run makefile

./cal.exe < cal.in > f # Input and output redirection
./cal.exe < cal.in | wc >> f # Piping output from executable to 'wc' and appending it to output file 'f'

cat f # Display results on terminal window
