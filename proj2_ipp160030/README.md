# CS 5348: Project 2 #

### Part 1 Summary ###

Part 1 compiles without any errors. I'm not sure why I get some out-page errors from time to time, and there was nothing in the simOS system code that helped me better diagnose my problem. 

### Part 2 Summary ###

Since the handout did not specify how many programs to make, I just made one new program for submission named 'prog2'. This is a much simpler program than 'prog1' and less random. The purpose of this program is to demonstrate simple 'load', 'add' and 'store' functions. A random 'indx' is chosen. From that, an add and then a 'mult' operation are performed and stored to a completely different 'indx'. The program runs in simOS with minor out-page errors but no serious errors.

### Part 3 Summary ###

Whatever work was done for Phase III is included in a directory separate 
from Part 2. I set up the process of beginning the 'signal' and 'pipe' commands for Phase 3, but was unable to proceed with them due to spending a lot of time with Phase I of the project. I did, however, manage to get a working 'fork()' for the child process and achieve input that would later be piped to the child simOS process. 

### Execution Instructions ###

1) To run Phase I, simply type 'make' into the command line. This will create simos.exe.
2) To execute simos.exe, simply type './simos.exe'.
3) Proceed with inputting commands like normal.
4) For Phase II, feel free to try what you did for Phase I with 'prog2'.
5) To run part of Phase III, you first need to go into the 'proj2_phase3' directory.
6) Once you're in the directory I just mentioned, you can simply use the shell script file I created to get a somewhat working product (i.e. use 'bash scommands.sh').
