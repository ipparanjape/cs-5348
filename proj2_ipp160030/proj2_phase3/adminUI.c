#include <stdio.h> 
#include <unistd.h>
#include <string.h>

int main ()
{
   // Copied from template provided from
   // https://personal.utdallas.edu/~ilyen/course/os/post-code/pipe.c.txt
   char buf[1024];
   int fds1[2], fds2[2];
   int childr, childw, parentr, parentw;
   int pid, iter; // 'iter' is 'numR' as defined in the handout
   FILE *infF;

   childr = fds1[0];
   childw = fds2[1];
   parentr = fds2[0];
   parentw = fds1[1];

   pid = fork();
   while (pid != 0) {continue;} // Waits until a child process is created
   if (pid == 0)
   {
      printf("Enter a program submission filename: ");
      scanf("%s", &buf);
      printf("Number of rounds/iterations? ");
      scanf("%d", &iter);
      // Compiles makefile as it is relevant to Part 3 only
      char cmd1[100], cmd2[100];
      strcpy(cmd1, "make simos3.exe");
      system(cmd1);
      // strcpy(cmd2, "./simos3.exe");
      // system(cmd2);
   }
   return 0;
}


