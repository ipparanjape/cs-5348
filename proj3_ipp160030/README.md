# CS 5348: Project 3 #

### Part 1 Summary ###

I spent a lot of this project correcting my cpu.c and process.c code
and makefile so that this part could run something of substance. Threads
are successfully created for term.c and processes are written to 
terminal.out. Synchronization is not perfect, however, because you can run
into an infinite loop. Nevertheless, you can execute a sample program
and get some results in 'terminal.out.' 

Summary of changes:

* cpu.c - I realized that I was missing some steps for some of the CPU code
from Project 2, so I spent a lot of time fixing it for this project. 
With the term.o from Project 2, this file should now run almost perfectly.

* process1.c - Similar explanation as cpu.c. I realized that I was writing 
some code wrong, and I fixed it from Project 2. Note that 'process1' indicates
that this code is ONLY applicable for Phase I, since we had to make some
changes to 'process.c' for Phase II. 

* term.c - I used two semaphores to synchronize the threads. The threads are 
synchronized well up to a point (you will see output in 'terminal.out'), 
but after that, something happened. I didn't have enough time to fix it, 
unfortunately.

* makefile - I realized that probably the biggest reason I was getting 
errors for Project 2 was that I should have commented out the idle.o process.
Although the instructions say that the file is complete, when I ran it through
the makefile, I was stuck with an idle pid. I will probably talk to you about
this in an office hour. Also, I added a few lines of code for 'process1.o' 
so that you can focus on this part rather than the 'process.o' code for Phase
II. You will have to do some commenting and uncommenting though, since the 
makefile is currently structured to run Phase II.

* server.c, client.c, sockets\_ex.sh - These files were just kept in this
directory to assist me with Phases III and IV. They were not necessary for
the implementation of this phase.

### Part 2 Summary ###

Although I declared three integer semaphores (accX, accY, accZ) for this
part, I only used one. The code seems to work similar to how it does for
Part 1. Since the instructions were a bit vague, I gave the insert termIO 
function priority over moveto ready function with just one semaphore: accX.
I declared more semaphores thinking I would need them, but I don't think
I need them at this time.

Summary of changes:

* process.c - I tried using one semaphore to give priority to the
insert\_termIO function. I think more semaphores should have been used,
but this is what I have. Also note that this is NOT the same process.c 
file I used for Part 1. I have included a separate file ('process1.c') for
that. 

### Parts 3&4 Summary ###

I have created a new directory called 'proj3\_phase3-4' in which you can 
run code for Phases III and IV. You MUST run the makefile in this directory
and run the server and client programs in this directory for the program
to work due to the significant changes I have created.

Summary of what I have done:

* Part (A): The 's' command is commented out, and client submissions are now
handled by submit\_server.c, which is derived from submit.c from Phases I & II.
Since the cpu.c code containing submit interrupt handles was not given, 

* Part (B): The cpu.o and simos.h files Professor Yen provided us in
the handout Section 3.6 contain no information about how to set a submitInterrupt (I tried running it that way and commented it out because it wouldn't work). 
Instead, I submitted the program through the server program. 

* Part (C): I made a client.c program with a lot of code refactored from 
Professor yen's website: 
https://personal.utdallas.edu/~ilyen/course/os/home.html

I only chaged a few parts that were relevant to this phase of the project.
Instead of having the client say 's prog1', I prompted the client for what
program he or she would like to submit. I wrote the client to close after 
just one instance of asking for program input, but I might change it up a 
little if I still have time remaining. 

* Part (D): The "terminal\_output" function is changed only for this 
directory so that the client can say any output that is normally given to 
terminal.out. 

* Part (E): Use an IP address of 127.0.0.1 and any port number >=5000, as
long as the port numbers for simos.exe and client.exe are the same.

### Phase IV ###

This part has not been done yet.

### Execution Instructions ###

1) To run Phase I and II, simply type 'make' into the command line. This will create simos.exe.
2) To execute simos.exe, simply type './simos.exe'.
3) Proceed with inputting commands like normal.

5) To run Phase III, you first need to go into the 'proj2\_part3-4' directory.
6) The server executable will just be 'simos.exe' Type 'make' to get 
the executable.
7) The client executable will be 'client.exe'. Type 'make client.exe' to get
the executable.
8) Execute ./simos.exe and ./client.exe in different windows.
9) For simos.exe, give arguments of numR and port number 
(e.g. ./simos.exe 1 5500)
10) For client.exe give arguments of an IP address and port number. Use
127.0.0.1 as the IP address. (e.g. ./client.exe 127.0.0.1 5500)
11) Make sure the port numbers for the server and client are the same.
