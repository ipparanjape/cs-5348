#include "simos.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

//===============================================================
// The interface to interact with clients for program submission
// --------------------------
// Should change to create server socket to accept client connections
// -- Best is to use the select function to get client inputs
// Should change term.c to direct the terminal output to client terminal
//===============================================================

void program_submission ()
{ char fname[100];

  fprintf (infF, "Submission file: ");
  scanf ("%s", &fname);
  if (uiDebug) fprintf (bugF, "File name: %s has been submitted\n", fname);
  submit_process (fname);
}

void main (int argc, char* argv[])
{
   systemActive = 1;

   initialize_system ();
   process_admin_commands ();
   
   
   int sockfd, newsockfd, portno, clilen;
   char buffer[256];
   struct sockaddr_in serv_addr, cli_addr; 
   int ret;

   if (argc < 3)
   {
      fprintf(stderr, "ERROR, no numR or port provided\n");
      exit(1);
   }

   /* Note that the code below is copied from the server.c code
    * provided at https://personal.utdallas.edu/~ilyen/course/os/home.html
    */
   
   sockfd = socket(AF_INET, SOCK_STREAM, 0); // Establishes TCP connection
   if (sockfd < 0) {error("ERROR opening socket");}
   bzero ((char *) &serv_addr, sizeof(serv_addr));
   portno = atoi(argv[1]);
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = INADDR_ANY;
   serv_addr.sin_port = htons(portno);
   if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
   {
      error("ERROR binding");
   }

   listen(sockfd,5);
   clilen = sizeof(cli_addr);
   newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
   if (newsockfd < 0) {error ("ERROR accepting");}
   else {printf ("Accept client socket %d, %d\n", newsockfd,\ 
   (int)cli_addr.sin_port);}

   bzero(buffer,256);
   ret = read (newsockfd, buffer, 255);
   if (ret < 0) {error ("ERROR reading from socket");}
   printf ("Here is the message: %s\n", buffer);
   ret = write (newsockfd, "I got your message", 18);
   if (ret < 0) error ("ERROR writing to socket");

   close (newsockfd);

   fprintf (infF, "System exiting!!!\n");
   system_exit ();
}   

	  

 

